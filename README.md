# 基于 Vue 3 + Typescript + Vite开发的低代码框架

### 强烈推荐使用 yarn 作为包管理

1. 安装依赖
    ``` shell
	yarn 
	```
2. 运行项目
    ``` shell
	yarn dev
	```

### 使用 npm 管理

1. 安装依赖
    ``` shell
	npm install
	```
2. 运行项目
    ``` shell
	npm run dev
	```
---

#### 组件放在/src/modules中，已对该文件夹做了批量导入

- [组件开发文档](https://v3.cn.vuejs.org/)
> 让框架兼容组件，而不是让组件兼容框架


---

#### 目前使用的技术栈

- 编程语言：[TypeScript 4.x](https://www.typescriptlang.org/zh/) + [JavaScript](https://www。javascript.com/)
- 构建工具：[Vite 2.x](https://cn.vitejs.dev/)
- 前端框架：[Vue 3.x](https://v3.cn.vuejs.org/)
- HTML文档预编译：[pug](https://pugjs.org/)

---

#### 功能清单

- [x] 没大问题的核心算法
- [ ] 导入json后结构还原
- [ ] 图形编辑页面
- [ ] 一键打包功能

---

### 欢迎各位大佬加入本项目
