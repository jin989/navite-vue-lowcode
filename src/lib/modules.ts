import { defineAsyncComponent } from "@vue/runtime-core";
// 基础模块
const Modules = (() => {
  const modules: any = {},
    comps = import.meta.glob("/src/modules/**/*.vue");
  Object.keys(comps).forEach((key) => {
    const name = key.match(/\/src\/modules\/(.+)\.vue/)?.[1];
    name && (modules[name] = defineAsyncComponent(comps[key]));
  });
  return modules;
})();
export default Modules